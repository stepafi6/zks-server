package com.travelbuddies.server.service;

import com.travelbuddies.server.dao.DestinationDao;
import com.travelbuddies.server.model.Country;
import com.travelbuddies.server.model.Destination;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import static org.junit.jupiter.api.Assertions.*;


import java.util.List;

@Transactional
@SpringBootTest
public class DestinationServiceTest {

    @Autowired
    private DestinationDao destinationDao;

    @Autowired
    private DestinationService destinationService;

    @BeforeEach
    public void setUp() {
        Country countryCzechia = new Country();
        countryCzechia.setId(0);
        countryCzechia.setName("Czechia");
        countryCzechia.setAbbreviation("CZ");

        Country countrySlovakia = new Country();
        countrySlovakia.setId(1);
        countrySlovakia.setName("Slovakia");
        countrySlovakia.setAbbreviation("SK");

        Destination destinationPrague = new Destination();
        destinationPrague.setGeonameId(0);
        destinationPrague.setCountry(countryCzechia);
        destinationPrague.setCity("Praha");
        destinationPrague.setSubcountry("Praha");
        destinationDao.save(destinationPrague);

        Destination destinationBrno = new Destination();
        destinationBrno.setGeonameId(1);
        destinationBrno.setCountry(countryCzechia);
        destinationBrno.setCity("Brno");
        destinationBrno.setSubcountry("Jihomoravský");
        destinationDao.save(destinationBrno);

        Destination destinationBratislava = new Destination();
        destinationBratislava.setGeonameId(2);
        destinationBratislava.setCountry(countrySlovakia);
        destinationBratislava.setCity("Bratislava");
        destinationBratislava.setSubcountry("Bratislavský");
        destinationDao.save(destinationBratislava);
    }

    @Test
    void testAutocompleteCity(){
        List<Destination> res = destinationService.autocompleteDestinationCity("Br");

        assertEquals(2,res.size());
        assertTrue(res.stream().map(Destination::getCity).anyMatch(x -> x.equals("Brno")));
        assertTrue(res.stream().map(Destination::getCity).anyMatch(x -> x.equals("Bratislava")));
    }

    @Test
    void testAutocompleteSubCountry(){
        List<Destination> res = destinationService.autocompleteDestinationSubCountry("J");

        assertEquals(1,res.size());
        assertEquals("Brno",res.stream().findFirst().get().getCity());
    }

    @Test
    void testAutocomplete(){
        List<Destination> res = destinationService.autocompleteDestination("ský");

        assertEquals(2,res.size());
        assertTrue(res.stream().map(Destination::getCity).anyMatch(x -> x.equals("Brno")));
        assertTrue(res.stream().map(Destination::getCity).anyMatch(x -> x.equals("Bratislava")));
    }

    @Test
    void testAutocompleteCityEmpty(){
        List<Destination> res = destinationService.autocompleteDestinationCity("Ostr");

        assertEquals(0,res.size());
    }

    @Test
    void testAutocompleteSubCountryEmpty(){
        List<Destination> res = destinationService.autocompleteDestinationSubCountry("Libe");

        assertEquals(0,res.size());
    }

    @Test
    void testAutocompleteEmpty(){
        List<Destination> res = destinationService.autocompleteDestination("Karl");

        assertEquals(0,res.size());
    }

}
