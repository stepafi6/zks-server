package com.travelbuddies.server.service;

import com.travelbuddies.server.dao.ConversationDao;
import com.travelbuddies.server.model.Conversation;
import com.travelbuddies.server.model.Message;
import com.travelbuddies.server.model.SystemUser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
public class MessageServiceTest {

    @Autowired
    private MessageService messageService;
    @Autowired
    private SystemUserService systemUserService;

    @BeforeEach
    public void setUp() {
        user = new SystemUser();
        user.setId("userA");
        user = systemUserService.saveUser(user);
    }

    SystemUser user;

    @Test
    void sendMessageAddsMessageToConversation() {
        final Conversation conversation = new Conversation();
        final String message = "Lorem ipsum";

        assertNull(conversation.getMessages());

        messageService.sendMessage(message, conversation, user);

        assertTrue(conversation.getMessages().stream().map(Message::getContent).anyMatch(x -> x.equals(message)));
    }

    @Test
    void createConversationCreatesConversationWithSpecifiedUsers() {
        SystemUser userB = new SystemUser();
        userB.setId("userB");
        userB = systemUserService.saveUser(userB);

        List<SystemUser> users = new ArrayList<>();
        users.add(user);
        users.add(userB);

        messageService.createConversation(users);

        List<Conversation> conversations = messageService.getUserConversations(user);

        assertTrue(conversations.stream().anyMatch(conv -> conv.systemUsers.stream().anyMatch(usr -> usr.getId().equals("userB"))));
    }

    @Test
    void getConversationMessages() {
        final Conversation conversation = new Conversation();

        messageService.sendMessage("Lorem ipsum", conversation, user);
        messageService.sendMessage("dolor san", conversation, user);

        List<Message> messages = messageService.getConversationMessages(conversation);

        assertEquals(2, messages.size());
    }


}
