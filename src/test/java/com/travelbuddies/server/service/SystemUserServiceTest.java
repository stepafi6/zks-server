package com.travelbuddies.server.service;

import com.travelbuddies.server.dao.CountryDao;
import com.travelbuddies.server.dao.SystemUserDao;
import com.travelbuddies.server.model.Country;
import com.travelbuddies.server.model.SystemUser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@SpringBootTest
class SystemUserServiceTest {

    @Mock
    SystemUserDao systemUserDao;

    @Mock
    CountryDao countryDao;

    @InjectMocks
    private SystemUserService systemUserService;

    @BeforeEach
    public void setUp() {

    }

    @Test
    void addFriend() {
        SystemUser user = new SystemUser();
        SystemUser friendToBeAdded = new SystemUser();
        systemUserService.addFriend(user,friendToBeAdded);
        assertTrue(user.getFriends().stream().anyMatch(f -> f.equals(friendToBeAdded)));
        assertTrue(friendToBeAdded.getFriends().stream().anyMatch(f -> f.equals(user)));
    }

    @Test
    void deleteFriend() {
        SystemUser user = new SystemUser();
        SystemUser friend = new SystemUser();
        systemUserService.addFriend(user,friend);
        assertTrue(user.getFriends().stream().anyMatch(f -> f.equals(friend)));
        systemUserService.deleteFriend(user,friend);
        assertFalse(friend.getFriends().stream().anyMatch(f -> f.equals(user)));
    }

    @Test
    void addVisitedCountry() {
        Country country = new Country();
        country.setId(1);
        country.setName("Imperium");
        when(countryDao.findById(anyLong())).thenReturn(java.util.Optional.of(country));

        SystemUser user = new SystemUser();

        systemUserService.addVisitedCountry(user, country.getId());
        assertTrue(user.getVisitedCountries().contains(country));
    }

    @Test
    void getNotVisitedCountries() {
        Country countryA = new Country();
        countryA.setId(1);
        countryA.setName("Imperium");

        Country countryB = new Country();
        countryA.setId(2);
        countryA.setName("Republic");

        List<Country> countries = new ArrayList<>();
        countries.add(countryA);
        countries.add(countryB);

        when(countryDao.findAll()).thenReturn(countries);

        Set<Country> visited = new HashSet<>();
        visited.add(countryA);
        List<Country> notVisited = systemUserService.getNotVisitedCountries(visited);

        assertTrue(notVisited.stream().allMatch(x -> x.equals(countryB)));
    }
}