package com.travelbuddies.server.service;

import com.travelbuddies.server.dao.DestinationDao;
import com.travelbuddies.server.model.Destination;
import com.travelbuddies.server.model.Event;
import com.travelbuddies.server.model.SystemUser;
import org.joda.time.DateTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
class EventServiceTest {

    @Autowired
    private EventService eventService;

    @Autowired
    private SystemUserService systemUserService;

    @Autowired
    private DestinationDao destinationDao;

    SystemUser userAuthor;
    Event event;

    SimpleDateFormat parser = new SimpleDateFormat("dd.MM.yyyy");

    @BeforeEach
    public void setUp() throws ParseException {
        userAuthor = new SystemUser();
        userAuthor.setId("userCreator");
        userAuthor = systemUserService.saveUser(userAuthor);

        Destination destinationPrague = new Destination();
        destinationPrague.setCity("Praha");
        destinationPrague = destinationDao.save(destinationPrague);

        event = new Event();
        event.setDescription("Test event");
        event.setAuthor(userAuthor);
        event.setFromDate(parser.parse("01.06.2021"));
        event.setToDate(parser.parse("01.07.2021"));
        event.setDestination(destinationPrague);

        event = eventService.createEvent(event);
    }

    @Test
    void addParticipant() {
        SystemUser user = new SystemUser();
        user.setId("userParticipantA");
        systemUserService.saveUser(user);

        eventService.addParticipant(event, user);
        assertTrue(event.getParticipants().stream().map(SystemUser::getId).anyMatch(usr -> usr.equals("userParticipantA")));
    }

    @Test
    void removeParticipant() {
        SystemUser user = new SystemUser();
        user.setId("userParticipantB");
        systemUserService.saveUser(user);

        eventService.addParticipant(event, user);

        assertTrue(event.getParticipants().stream().map(SystemUser::getId).anyMatch(usr -> usr.equals("userParticipantB")));

        eventService.removeParticipant(event, user);
        assertFalse(event.getParticipants().stream().map(SystemUser::getId).anyMatch(usr -> usr.equals("userParticipantB")));
    }

    @Test
    void getAllUserEvents() {
        Event event2 = new Event();
        event2.setDescription("Test event 2");
        event2.setAuthor(userAuthor);
        eventService.createEvent(event2);

        List<Event> events = eventService.getAllUserEvents(userAuthor);

        assertEquals(2, events.size());
    }

    @Test
    void searchEvents() throws ParseException {
        Calendar cal = Calendar.getInstance();
        cal.setTime(parser.parse("15.06.2021"));
        List<Event> events = eventService.searchEvents(cal, event.getDestination(), 30);

        assertEquals(1, events.size());
    }

    @Test
    void upcomingEvents() throws ParseException {
        Date now = new Date();
        now.setTime(System.currentTimeMillis());

        Event upcomingEvent = new Event();
        upcomingEvent.setDescription("Test upcoming event");
        upcomingEvent.setAuthor(userAuthor);
        upcomingEvent.setFromDate(new DateTime(now).plusDays(7).toDate());
        upcomingEvent.setToDate(new DateTime(now).plusDays(14).toDate());

        eventService.addParticipant(upcomingEvent, userAuthor);

        List<Event> events = eventService.getAllUserUpcomingEvents(userAuthor);

        assertTrue(events.stream().map(Event::getDescription).anyMatch(e -> e.equals("Test upcoming event")));
    }

    @Test
    void getAllUserEventsToCity() {
        List<Event> events = eventService.getAllUserEventsToCity(userAuthor, "Praha");

        assertEquals(1, events.size());
        assertTrue(events.stream().map(Event::getDescription).anyMatch(dsc -> dsc.equals("Test event")));
    }

    @Test
    void deleteEvent() {
        eventService.deleteEvent(event);

        assertTrue(event.isDeleted());
    }
}