package com.travelbuddies.server.dao;


import com.travelbuddies.server.model.Country;
import org.springframework.data.hazelcast.repository.HazelcastRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryDao extends HazelcastRepository<Country,Long> {
    Country findByName(String name);
    Country findByAbbreviation(String abbreviation);
}
