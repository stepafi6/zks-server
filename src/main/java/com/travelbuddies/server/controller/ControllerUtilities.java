package com.travelbuddies.server.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.travelbuddies.server.dao.SystemUserDao;
import com.travelbuddies.server.exception.NotFoundException;
import com.travelbuddies.server.model.SystemUser;
import com.travelbuddies.server.service.SystemUserService;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@Component
public class ControllerUtilities {
    private OkHttpClient client;
    private ObjectMapper mapper;

    private SystemUserService systemUserService;

    private static final Logger LOG = LoggerFactory.getLogger(ControllerUtilities.class);

    @Autowired
    public ControllerUtilities(SystemUserService systemUserService) {
        this.systemUserService = systemUserService;

        client = new OkHttpClient().newBuilder()
                .build();

        mapper = new ObjectMapper();
    }


    /**
     * Uses Auth0 API to fetch informations about callers identity.
     *
     * @param token Access token of the systemUser.
     * @return Mapped systemUser model.
     */
    public SystemUser GetUser(JwtAuthenticationToken token) {

        Jwt jwt = token.getToken();
        String userId = jwt.getClaim("sub");
        SystemUser dbUser = null;
        try {
            dbUser = systemUserService.findById(userId);
        } catch (NotFoundException nfE) {
            try {
                //Prepare request for the API and add access token as Bearer authorization
                LOG.info("Fetching user info");
                Request request = new Request.Builder()
                        .url("https://travel-buddies.eu.auth0.com/userinfo")
                        .method("GET", null)
                        .addHeader("Authorization", "Bearer " + jwt.getTokenValue())
                        .build();
                Response response = client.newCall(request).execute();
                String responseText = response.body().string();
                LOG.info(responseText);

                //Use object mapper to map JSON to model
                dbUser = mapper.readValue(responseText, SystemUser.class);
                systemUserService.saveUser(dbUser);
            } catch (Exception e) {
                LOG.error(e.getMessage());
            }
        }

        return dbUser;
    }

    public static HttpHeaders createLocationHeaderFromCurrentUri(String path, Object... uriVariableValues) {
        assert path != null;

        final URI location = ServletUriComponentsBuilder.fromCurrentRequestUri().path(path).buildAndExpand(
                uriVariableValues).toUri();
        final HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.LOCATION, location.toASCIIString());
        return headers;
    }
}
