package com.travelbuddies.server.exception;

public class NegativeRangeException extends BaseException {
    public NegativeRangeException() {
    }

    public NegativeRangeException(String message) {
        super(message);
    }

    public NegativeRangeException(String message, Throwable cause) {
        super(message, cause);
    }

    public NegativeRangeException(Throwable cause) {
        super(cause);
    }
}
