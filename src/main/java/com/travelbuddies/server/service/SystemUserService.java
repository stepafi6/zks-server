package com.travelbuddies.server.service;

import com.travelbuddies.server.dao.CountryDao;
import com.travelbuddies.server.dao.SystemUserDao;
import com.travelbuddies.server.exception.NotFoundException;
import com.travelbuddies.server.model.Country;
import com.travelbuddies.server.model.SystemUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Set;

@Service
public class SystemUserService {

    private final SystemUserDao systemUserDao;
    private final CountryDao countryDao;

    @Autowired
    public SystemUserService(SystemUserDao systemUserDao, CountryDao countryDao) {
        this.systemUserDao = systemUserDao;
        this.countryDao = countryDao;
    }

    /**
     * Adds friend to user's friends list and conversely.
     *
     * @param user           SystemUser to which the new friend should be added.
     * @param userToBeFriend SystemUser to be added as an new friend to user's friend list.
     */
    @Transactional
    public void addFriend(SystemUser user, SystemUser userToBeFriend) {
        Objects.requireNonNull(user);
        Objects.requireNonNull(userToBeFriend);
        user.addFriend(userToBeFriend);
        userToBeFriend.addFriend(user);
        systemUserDao.save(userToBeFriend);
        systemUserDao.save(user);
    }

    /**
     * Deletes friend from user's friend list and conversely.
     *
     * @param user              SystemUser from which friend list a friend should be deleted.
     * @param friendToBeDeleted SystemUser to be deleted from friend list.
     */
    @Transactional
    public void deleteFriend(SystemUser user, SystemUser friendToBeDeleted) {
        Objects.requireNonNull(user);
        Objects.requireNonNull(friendToBeDeleted);
        user.removeFriend(friendToBeDeleted);
        friendToBeDeleted.removeFriend(user);
        systemUserDao.save(user);
        systemUserDao.save(friendToBeDeleted);
    }


    /**
     * Adds country to user's list of visited countries.
     *
     * @param user    SystemUser who has visited a new country.
     * @param countryId Country which should be added to user's visited countries.
     */
    @Transactional
    public void addVisitedCountry(SystemUser user, long countryId) {
        Objects.requireNonNull(user);

        Country country = countryDao.findById(countryId).orElseThrow(com.travelbuddies.server.exception.NotFoundException::new);
        user.addVisitedCountry(country);
        systemUserDao.save(user);
    }

    /**
     * Returns list of all system users.
     * @return List of all system users.
     */
    @Transactional(readOnly = true)
    public List<SystemUser> getAll() {
        return systemUserDao.findAll();
    }

    /**
     * Gets user by ID
     * @param id ID of the searched user
     * @return User with wanted ID
     */
    @Transactional(readOnly = true)
    public SystemUser getSystemUser(String id) {
        return systemUserDao.findById(id).orElse(null);
    }

    /**
     * Gets user by ID
     * @param friendId ID of the searched user
     * @return User with wanted ID
     */
    @Transactional
    public SystemUser findById(String friendId) {
        return systemUserDao.findById(friendId).orElseThrow(NotFoundException::new);
    }

    /**
     * Saves user to database
     * @param user User to be saved
     * @return Saved user
     */
    @Transactional
    public SystemUser saveUser(SystemUser user) {
        return systemUserDao.save(user);
    }

    /**
     * Gets a list of users based upon a query.
     * @param query Query which is used for searching the user. Could be the first or last name
     * @return List of users based upon a query
     */
    public List<SystemUser> searchUsers(String query) {
        return systemUserDao.findByGivenNameContainingOrFamilyNameContainingIgnoreCase(query,query);
    }

    /**
     * Gets a list of countries which have not been visited by currently logged user
     * @param visitedCountries Countries which user have already visited
     * @return List of countries which user have not visited
     */
    @Transactional(readOnly = true)
    public List<Country> getNotVisitedCountries(Set<Country> visitedCountries) {
        List<Country> allCountries = (List<Country>) countryDao.findAll();
        allCountries.removeAll(visitedCountries);
        return allCountries;
    }
}
