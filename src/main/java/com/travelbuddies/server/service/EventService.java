package com.travelbuddies.server.service;

import com.travelbuddies.server.dao.EventDao;
import com.travelbuddies.server.exception.NotFoundException;
import com.travelbuddies.server.model.Destination;
import com.travelbuddies.server.model.Event;
import com.travelbuddies.server.model.SystemUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.*;

@Service
public class EventService {


    private final EventDao eventDao;

    @Autowired
    public EventService(EventDao eventDao) {
        this.eventDao = eventDao;
    }

    /**
     * Creates an event.
     *
     * @param event Event to be created.
     */
    @Transactional
    public Event createEvent(Event event) {
        Objects.requireNonNull(event);

        if (event.getParticipants() == null)
            event.setParticipants(new HashSet<>());

        return eventDao.save(event);
    }

    /**
     * Lists all events user is participating in
     *
     * @param user logged in user.
     * @return All events user is participating in
     */
    @Transactional(readOnly = true)
    public List<Event> getAllUserEvents(SystemUser user) {
        Objects.requireNonNull(user);
        return eventDao.findByParticipantsContainingIgnoreCaseOrAuthor(user,user);
    }

    /**
     * Gets all events to a country which user has attended
     * @param user User who has attended the events
     * @param country Country to which we want to find events
     * @return All events to country which user has attended
     */
    @Transactional(readOnly = true)
    public List<Event> getAllUserEventsToCountry(SystemUser user, String country)
    {
        Objects.requireNonNull(user);
        return eventDao.findAllByDestination_Country_WhereIsUser(country,user);
    }

    /**
     * Gets all events to a city which user has attended
     * @param user User who has attended the events
     * @param city City to which we want to find events
     * @return All events to a city which user has attended
     */
    @Transactional(readOnly = true)
    public List<Event> getAllUserEventsToCity(SystemUser user, String city)
    {
        Objects.requireNonNull(user);
        return eventDao.findAllByDestination_CityWhereIsUser(city,user);
    }

    /**
     * Gets all upcoming events for user
     * @param user User for who we want to find upcoming events
     * @return All upcoming events for user
     */
    @Transactional(readOnly = true)
    public List<Event> getAllUserUpcomingEvents(SystemUser user) {
        Objects.requireNonNull(user);

        Date date=Calendar.getInstance().getTime();
        return eventDao.findAllByParticipantsContainingIgnoreCaseAndFromDateAfterOrAuthorAndFromDateAfterOrderByFromDateAsc(user,date,user,date);
    }

    /**
     * Get event by id
     *
     * @param id of event.
     * @return Event with wanted ID
     */
    @Transactional(readOnly = true)
    public Event getEvent(long id) {
        return eventDao.findById(id).orElse(null);
    }

    /**
     * Sets the event's deleted flag to true.
     *
     * @param event Event to be deleted.
     */
    @Transactional
    public void deleteEvent(Event event) {
        Objects.requireNonNull(event);
        event.setDeleted(true);
        eventDao.save(event);
    }

    /**
     * Updates an event.
     *
     * @param event Event to be updated.
     */
    @Transactional
    public void updateEvent(Event event) {
        Objects.requireNonNull(event);
        eventDao.save(event);
    }

    /**
     * Adds the participant to the specified event.
     *
     * @param event      Event to which the user should be added.
     * @param systemUser Participant to be added to the event.
     */
    @Transactional
    public void addParticipant(Event event, SystemUser systemUser) {
        Objects.requireNonNull(event);
        Objects.requireNonNull(systemUser);
        event.getParticipants().add(systemUser);
        eventDao.save(event);
    }

    /**
     * Removes the participant from the specified event.
     *
     * @param event      Event from which the user should be removed.
     * @param systemUser Participant to be removed from the event.
     */
    @Transactional
    public void removeParticipant(Event event, SystemUser systemUser) {
        Objects.requireNonNull(event);
        Objects.requireNonNull(systemUser);
        event.getParticipants().remove(systemUser);
        eventDao.save(event);
    }

    /**
     * Get all events
     *
     * @return list of events
     */
    @Transactional(readOnly = true)
    public List<Event> getAll() {
        return eventDao.findAll();
    }

    /**
     * Gets all element that match criteria
     *
     * @param date        when you want to go
     * @param destination where you want to go
     * @param ramge       +-days to search
     * @return            All events that match criteria
     */
    @Transactional(readOnly = true)
    public List<Event> searchEvents(Calendar date, Destination destination, Integer ramge) {
        date.add(Calendar.DATE, -ramge);
        Date from = date.getTime();
        date.add(Calendar.DATE, 1 + 2 * ramge);
        Date to = date.getTime();

        return eventDao.findAllByDestinationAndFromDateBetween(destination, from, to);

    }

    /**
     * Adds user to an event
     * @param id ID of the event to which user should be attended
     * @param user User which is going to be added to the event
     * @return Event to which user has been added
     */
    public Event attendEvent(Long id, SystemUser user) {
        Event event = eventDao.findById(id).orElseThrow(NotFoundException::new);

        event.getParticipants().add(user);

        eventDao.save(event);

        return event;
    }
}
