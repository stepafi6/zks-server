package com.travelbuddies.server.service;

import com.travelbuddies.server.dao.CountryDao;
import com.travelbuddies.server.model.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class CountryService {
    private final CountryDao countryDao;

    @Autowired
    public CountryService(CountryDao countryDao) {
        this.countryDao = countryDao;
    }

    /**
     * Returns country from name
     * @param name Name of the country
     * @return Country             
     */
    @Transactional Country getCountry(String name)
    {
        return countryDao.findByName(name);
    }




}


