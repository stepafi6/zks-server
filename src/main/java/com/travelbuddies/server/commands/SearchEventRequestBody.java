package com.travelbuddies.server.commands;

import com.travelbuddies.server.model.Destination;

public class SearchEventRequestBody {
    Destination destination;
    int year;
    int month;
    int day;
    int rangeInDays;

    public SearchEventRequestBody(Destination destination, int year, int month, int day, int rangeInDays) {
        this.destination = destination;
        this.year = year;
        this.month = month;
        this.day = day;
        this.rangeInDays = rangeInDays;
    }

    public int getRangeInDays() {
        return rangeInDays;
    }

    public void setRangeInDays(int rangeInDays) {
        this.rangeInDays = rangeInDays;
    }

    public Destination getDestination() {
        return destination;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }
}
