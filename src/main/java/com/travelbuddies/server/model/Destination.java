package com.travelbuddies.server.model;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Document(indexName = "xxx")
@Table(name = "destination")
public class Destination implements Serializable {
    @Id
    private int geonameId;
    private String city;
    private String subcountry;

    @ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "country")
    private Country country;

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getGeonameId() {
        return geonameId;
    }

    public void setGeonameId(int geonameId) {
        this.geonameId = geonameId;
    }

    public String getSubcountry() {
        return subcountry;
    }

    public void setSubcountry(String subcountry) {
        this.subcountry = subcountry;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Destination that = (Destination) o;
        return geonameId == that.geonameId;
    }


    public boolean sameAs(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Destination that = (Destination) o;
        return geonameId == that.geonameId &&
                city.equals(that.city) &&
                subcountry.equals(that.subcountry) &&
                country.equals(that.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(geonameId);
    }
}